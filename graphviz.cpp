#include "graphviz.h"

Graphviz::Graphviz(std::string name)
{
    this->name = name;
    Node rootNode("graph");
    rootNode.setProp("layout", "dot");
    rootNode.setProp("rankdir", "TB");
    nodes.push_back(rootNode);


    /* Temp data

    Node one("one");
    one.setProp("label", "ahoj");

    Node two("two");
    two.setProp("shape", "square");

    Connection con;
    con.left.push_back(one);
    con.right.push_back(two);

    nodes.push_back(one);
    nodes.push_back(two);
    connections.push_back(con);

    End Temp data */
}

std::string Graphviz::toString()
{
    std::ostringstream output;

    output << "digraph " << name << " {" << std::endl;

    for (auto& node : nodes) {
        output << "\t" << node.toString() << std::endl;
    }

    for (auto& con : connections) {
        output << "\t" << con.toString() << std::endl;
    }

    output << "}" << std::endl;
    return output.str();
}

bool Graphviz::addNode(Node& node)
{
    for (auto& n : nodes) {
        if (n.name == node.name) {
            return false;
        }
    }
    nodes.push_back(node);
    return true;
}

bool Graphviz::setNode(Node& node)
{
    for (auto& n : nodes) {
        if (n.name == node.name) {
            n = node;
            return true;
        }
    }
    return false;
}
