#ifndef CONNECTION_H
#define CONNECTION_H

#include <string>
#include <vector>
#include <sstream>

#include "node.h"

class Connection
{
public:
    Connection();
    std::string toString();
    std::vector<Node> left;
    std::vector<Node> right;
};

#endif // CONNECTION_H
