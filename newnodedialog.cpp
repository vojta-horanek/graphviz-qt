#include "newnodedialog.h"
#include "ui_newnodedialog.h"



NewNodeDialog::NewNodeDialog(QWidget *parent, Node *node) :
    QDialog(parent),
    ui(new Ui::NewNodeDialog)
{
    ui->setupUi(this);

    QRegExp rx("^[a-zA-Z0-9]+$");
    validator = new QRegExpValidator(rx, this);
    ui->nodeNameInput->setValidator(validator);
    checkEmpty();

    if (node != nullptr) {
        ui->nodeNameInput->setText(QString::fromStdString(node->name));
        ui->nodeNameInput->setEnabled(false);
        nodeModel.reset(new NodeModel(this, false));
        for (auto& prop : node->properties) {
            nodeModel.get()->addRow(QString::fromStdString(prop.first),
                                    QString::fromStdString(prop.second));
        }
    } else {
        nodeModel.reset(new NodeModel(this));
    }
    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    pTableModel = dynamic_cast<QAbstractTableModel*>(nodeModel.data());
    ui->tableView->setModel(pTableModel);
}

NewNodeDialog::~NewNodeDialog()
{
    delete validator;
    delete ui;
}

Node NewNodeDialog::getNode()
{
    Node n(ui->nodeNameInput->text().toStdString());
    for(int i = 0; i < ui->tableView->model()->rowCount(); i++) {
        n.setProp(ui->tableView->model()->index(i, 0).data().toString().toStdString(),
                  ui->tableView->model()->index(i, 1).data().toString().toStdString());
    }
    return n;
}

void NewNodeDialog::on_buttonBox_accepted()
{
    result = true;
}

void NewNodeDialog::on_buttonBox_rejected()
{
    result = false;
}

void NewNodeDialog::on_nodeNameInput_textChanged(const QString &arg1)
{
    checkEmpty();
    QPushButton* butt = ui->buttonBox->button(QDialogButtonBox::Ok);
    if (arg1.toLower() == "node") {
        butt->setEnabled(false);
    } else {
        butt->setEnabled(true);
    }
}

void NewNodeDialog::checkEmpty() {
    QPushButton* butt = ui->buttonBox->button(QDialogButtonBox::Ok);
    if (ui->nodeNameInput->text().isEmpty()) {
        butt->setEnabled(false);
    } else {
        butt->setEnabled(true);
    }
}

void NewNodeDialog::on_addProp_clicked()
{
    nodeModel.get()->addEmptyRow();
}
