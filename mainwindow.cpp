#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    graphviz = new Graphviz("mGraph");
    graphPixmapItem = new QGraphicsPixmapItem;
    graphicsScene = new QGraphicsScene;

    graphicsScene->addItem(graphPixmapItem);
    ui->graphView->setScene(graphicsScene);

    generate();
}

MainWindow::~MainWindow()
{
    delete graphviz;
    delete graphPixmapItem;
    delete graphicsScene;
    delete ui;
}

void MainWindow::generate()
{
    if (graphviz->nodes.size() > 2)
        ui->actionNew_connection->setEnabled(true);

    std::string code = graphviz->toString();
    ui->output->setText(QString::fromStdString(code));

    graphPixmapItem->setPixmap(QPixmap::fromImage(System::getGraph(code)));

    ui->statusbar->showMessage(QString::number(graphviz->nodes.size() - 1) + " nodes, " +
                               QString::number(graphviz->connections.size()) + " connections");

    ui->nodeListView->clear();
    for (auto& node : graphviz->nodes) {
        ui->nodeListView->addItem(QString::fromStdString(node.name));
    }
}

void MainWindow::on_actionNew_node_triggered()
{    
    NewNodeDialog diag(this);
    diag.exec();
    if(diag.result) {
        Node node = diag.getNode();
        if (!graphviz->addNode(node)) {
            QMessageBox err;
            err.setText("Node already exists!");
            err.setIcon(QMessageBox::Critical);
            err.exec();
        } else {
            generate();
        }
    }
}

void MainWindow::on_actionNew_connection_triggered()
{
   NewConnectionDialog diag(this, graphviz->nodes);
   diag.exec();
   if (diag.result){
       if (diag.getFirstNode() == diag.getSecondNode()) {
            QMessageBox err;
            err.setText("Cannot connect same nodes together!");
            err.setIcon(QMessageBox::Critical);
            err.exec();
       } else {
           Connection con;
           con.left.push_back(diag.getFirstNode());
           con.right.push_back(diag.getSecondNode());
           graphviz->connections.push_back(con);
           generate();
       }
   }
}

void MainWindow::on_nodeListView_itemDoubleClicked(QListWidgetItem *item)
{
    NewNodeDialog diag(this,
                       &graphviz->nodes[ui->nodeListView->selectionModel()->selectedIndexes().first().row()]);
    diag.exec();
    if(diag.result) {
        Node node = diag.getNode();
        if (!graphviz->setNode(node)) {
            QMessageBox err;
            err.setText("Error edditing node");
            err.setIcon(QMessageBox::Critical);
            err.exec();
        } else {
            generate();
        }
    }

}

void MainWindow::on_actionExportGraph_triggered()
{
    QString fileName = QFileDialog::getSaveFileName(this,
        tr("Save Image"), ".svg", tr("Image Files (*.png *.jpg *.svg)"));

    if (!fileName.isEmpty()) {
        if (fileName.endsWith("png", Qt::CaseInsensitive)) {
           System::savePng(graphviz, fileName.toStdString());
        } else if (fileName.endsWith("svg", Qt::CaseInsensitive)) {
           System::saveSvg(graphviz, fileName.toStdString());
        } else {
           System::saveJpg(graphviz, fileName.toStdString());
        }
    }
}

void MainWindow::on_actionExportSource_triggered()
{
    QString fileName = QFileDialog::getSaveFileName(this,
        tr("Save Source"), ".gv", tr("Graph Files (*.gv)"));

    if (!fileName.isEmpty()) {
        QFile file(fileName);
        if (file.open(QIODevice::ReadWrite)) {
            QTextStream stream(&file);
            stream << QString::fromStdString(graphviz->toString());
        }
        file.close();
    }
}
