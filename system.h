#ifndef SYSTEM_H
#define SYSTEM_H

#include "graphviz.h"

#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>
#include <sstream>

#include <QImage>

class System
{
public:
    System();
    static std::string exec(const char* cmd);
    static QImage getGraph(std::string& source);
    static void savePng(Graphviz* graphviz, std::string filename);
    static void saveJpg(Graphviz* graphviz, std::string filename);
    static void saveSvg(Graphviz* graphviz, std::string filename);
};

#endif // SYSTEM_H
