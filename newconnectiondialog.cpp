#include "newconnectiondialog.h"
#include "ui_newconnectiondialog.h"

NewConnectionDialog::NewConnectionDialog(QWidget *parent, std::vector<Node> nodes) :
    QDialog(parent),
    ui(new Ui::NewConnectionDialog)
{
    ui->setupUi(this);
    this->nodes = nodes;
    for (auto& node : nodes) {
        if (node.name != "graph") {
            ui->boxFirst->addItem(QString::fromStdString(node.toString()));
            ui->boxSecond->addItem(QString::fromStdString(node.toString()));
        }
    }
}

NewConnectionDialog::~NewConnectionDialog()
{
    delete ui;
}

Node NewConnectionDialog::getFirstNode()
{
    return nodes[ui->boxFirst->currentIndex() + 1];
}

Node NewConnectionDialog::getSecondNode()
{
    return nodes[ui->boxSecond->currentIndex() + 1];
}

void NewConnectionDialog::on_buttonBox_accepted()
{
    result = true;
    close();
}

void NewConnectionDialog::on_buttonBox_rejected()
{
    result = false;
    close();
}
