#include "connection.h"

Connection::Connection()
{

}

std::string Connection::toString()
{
    std::ostringstream output;
    output << "{ " ;
    for (auto& node : left) {
        output << node.name << " ";
    }

    output << "} -> { ";

    for (auto& node : right) {
        output << node.name << " ";
    }
    output << "}";

    return output.str();
}
