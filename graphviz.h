#ifndef GRAPVIZ_H
#define GRAPVIZ_H

#include <vector>
#include <string>
#include <sstream>
#include <algorithm>

#include "node.h"
#include "connection.h"

class Graphviz
{
public:
    Graphviz(std::string name);
    std::vector<Node> nodes;
    std::vector<Connection> connections;
    std::string toString();
    bool addNode(Node& node);
    bool setNode(Node& node);
private:
    std::string name;
};

#endif // GRAPVIZ_H
