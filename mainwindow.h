#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "graphviz.h"
#include "newnodedialog.h"
#include "newconnectiondialog.h"
#include "system.h"

#include <iostream>
#include <QMessageBox>
#include <QGraphicsPixmapItem>
#include <QListWidget>
#include <QFileDialog>
#include <QTextStream>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_actionNew_node_triggered();

    void on_actionNew_connection_triggered();

    void on_nodeListView_itemDoubleClicked(QListWidgetItem *item);

    void on_actionExportGraph_triggered();

    void on_actionExportSource_triggered();

private:
    Ui::MainWindow *ui;
    Graphviz* graphviz;
    QGraphicsPixmapItem* graphPixmapItem;
    QGraphicsScene* graphicsScene;
    void generate();
};
#endif // MAINWINDOW_H
