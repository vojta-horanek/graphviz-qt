#ifndef NODE_H
#define NODE_H

#include <map>
#include <string>
#include <sstream>

class Node
{
public:
    Node(std::string name);
    void setProp(std::string name, std::string value);
    std::string toString();
    std::string name;
    bool operator== (const Node& n2);
    std::map<std::string, std::string> properties;
};

#endif // NODE_H
