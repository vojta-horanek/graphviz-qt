#ifndef NODEMODEL_H
#define NODEMODEL_H

#include <QAbstractTableModel>

class NodeModel : public QAbstractTableModel
{
public:
    NodeModel(QObject * pParent = Q_NULLPTR, bool init = true) : QAbstractTableModel(pParent)
    {
        if (init) {
            //populate default data
            QStringList row1;
            row1.append("shape");
            row1.append("square");
            m_arrData.append(row1);
        }
    }

    void addEmptyRow()
    {
        beginInsertRows(QModelIndex(), m_arrData.size()-1, m_arrData.size() - 1);
        QStringList row;
        row.append("");
        row.append("");
        m_arrData.append(row);
        endInsertRows();
    }

    void addRow(QString key, QString value)
    {
        beginInsertRows(QModelIndex(), m_arrData.size()-1, m_arrData.size() - 1);
        QStringList row;
        row.append(key);
        row.append(value);
        m_arrData.append(row);
        endInsertRows();
    }

    int rowCount(const QModelIndex &) const override
    {
        return m_arrData.size();
    }

    int columnCount(const QModelIndex &) const override
    {
        return 2;
    }

    QVariant data(const QModelIndex &index, int role) const override
    {
        if (role != Qt::DisplayRole && role != Qt::EditRole)
        {
            return QVariant();
        }
        return m_arrData[index.row()][index.column()];
    }

    QVariant headerData(int nSection, Qt::Orientation eOrientation, int nRole) const override
    {
        if (eOrientation != Qt::Horizontal || nRole != Qt::DisplayRole)
        {
            return QVariant();
        }
        switch (nSection)
        {
        case 0:
            return "Key";
        case 1:
            return "Value";
        default:
            return QVariant();
        }
    }

    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override
    {
        if (role == Qt::EditRole)
        {
            m_arrData[index.row()][index.column()] = value.toString();
        }
        return true;
    }

    Qt::ItemFlags flags(const QModelIndex &index) const override
    {
        if (!index.isValid())
        {
            return Qt::ItemIsEnabled;
        }

        return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;
    }

private:
    QList<QStringList> m_arrData;
};

#endif // NODEMODEL_H
