#ifndef NEWCONNECTIONDIALOG_H
#define NEWCONNECTIONDIALOG_H

#include <QDialog>

#include "node.h"
#include <vector>

namespace Ui {
class NewConnectionDialog;
}

class NewConnectionDialog : public QDialog
{
    Q_OBJECT

public:
     NewConnectionDialog(QWidget *parent, std::vector<Node> nodes);
    ~NewConnectionDialog();
    bool result = false;
    std::vector<Node> nodes;
    Node getFirstNode();
    Node getSecondNode();

private slots:
    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

private:
    Ui::NewConnectionDialog *ui;
};

#endif // NEWCONNECTIONDIALOG_H
