#include "node.h"

Node::Node(std::string name)
{
    this->name = name;
}

void Node::setProp(std::string name, std::string value)
{
    auto iter = properties.find(name);
    if (iter != properties.end()) {
        iter->second = value;
    } else {
        properties.insert(std::pair<std::string, std::string>(name, value));
    }
}

std::string Node::toString()
{
    std::ostringstream out;
    out << name;
    if (properties.size() != 0) {
        out << " [ ";
        for (auto& prop : properties) {
            if (prop.first != "" || prop.second != "")
                out << prop.first << "=\"" << prop.second << "\" ";
        }
        out << "]";
    }
    return out.str();
}

bool Node::operator== (const Node &n2)
{
    return this->name == n2.name;
}
