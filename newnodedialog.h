#ifndef NEWNODEDIALOG_H
#define NEWNODEDIALOG_H

#include <QDialog>
#include <QRegExpValidator>
#include <QPushButton>
#include <QHeaderView>

#include "nodemodel.h"
#include "qeditabletable.h"
#include "node.h"

namespace Ui {
class NewNodeDialog;
}

class NewNodeDialog : public QDialog
{
    Q_OBJECT

public:
    explicit NewNodeDialog(QWidget *parent = nullptr, Node *node = nullptr);
    ~NewNodeDialog();
    Node getNode();
    bool result = false;

private slots:
    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

    void on_nodeNameInput_textChanged(const QString &arg1);

    void on_addProp_clicked();

private:
    Ui::NewNodeDialog *ui;
    QRegExpValidator *validator;
    QSharedPointer<NodeModel> nodeModel;
    QAbstractTableModel *pTableModel;
    void checkEmpty();
};

#endif // NEWNODEDIALOG_H
