#include "system.h"

System::System()
{

}

std::string System::exec(const char *cmd)
{
    std::array<char, 128> buffer;
    std::string result;
    std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
    if (!pipe) {
        throw std::runtime_error("popen() failed!");
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
        result += buffer.data();
    }
    return result;
}

QImage System::getGraph(std::string& source)
{
    std::string command = "echo \"" + source + "\" | dot -Tsvg";
    QByteArray img = QByteArray::fromStdString(exec(command.c_str()));
    QImage result = QImage::fromData(img);
    return result;
}

void System::savePng(Graphviz* graphviz, std::string filename)
{
    std::string command = "echo \"" + graphviz->toString() + "\" | dot -Tpng > " + filename;
    exec(command.c_str());
}

void System::saveJpg(Graphviz* graphviz, std::string filename)
{
    std::string command = "echo \"" + graphviz->toString() + "\" | dot -Tjpg > " + filename;
    exec(command.c_str());
}

void System::saveSvg(Graphviz* graphviz, std::string filename)
{
    std::string command = "echo \"" + graphviz->toString() + "\" | dot -Tsvg > " + filename;
    exec(command.c_str());
}
